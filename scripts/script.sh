#!/bin/sh

set -ex

# Attribute	    Type        Required    Description
# name          string      yes         if path is not provided	The name of the new project. Equals path if not provided.
# path          string      yes         if name is not provided	Repository name for new project. Generated based on name if not provided (generated as lowercase with dashes).
# namespace_id  integer     no          Namespace for the new project (defaults to the current user’s namespace)

# Create Project with Shared Runners Enabled
GITLAB_PROJECT_ID=$(curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -X POST "$GITLAB_URL/api/v4/projects?path=$GITLAB_PROJECT&namespace_id=$GITLAB_NAMESPACE_ID" | jq .id)

# Add Environment Variables to Project
# curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/variables" --form "key=NEW_VARIABLE" --form "value=new value"

# Gitlab Users Access Levels
# https://docs.gitlab.com/ee/api/access_requests.html#valid-access-levels

# Add User to Project
GITLAB_USER_ID=$(curl -s --request GET --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/users?username=$GITLAB_USERNAME" | jq .[].id)
curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --data "user_id=$GITLAB_USER_ID&access_level=$GITLAB_USER_ACCESS_LEVEL" "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/members"